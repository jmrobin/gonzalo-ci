FROM openjdk:8-jdk-alpine

VOLUME /tmp

ADD /target/gonzalo-0.0.1-SNAPSHOT.jar webapp.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/webapp.jar"]
